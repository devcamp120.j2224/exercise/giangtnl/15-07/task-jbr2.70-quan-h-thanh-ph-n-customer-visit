import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class App {
    public static void main(String[] args) throws Exception {
        DateFormat dateFormat = null ;
        dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        Customer customer1 = new Customer();
        Customer customer2 = new Customer("gà",true,"gia cầm");

        System.out.println(customer1.toString());
        System.out.println(customer2.toString());
        Visit visit1 = new Visit(customer1, dateFormat, 20000,30000);
        Visit visit2 = new Visit(customer2, dateFormat, 50000,30000);
        System.out.println(visit1.toString());
        System.out.println(visit2.toString());
    }
}
