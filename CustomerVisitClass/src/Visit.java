import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Visit {
    Customer customer;
    Date date = new Date();
    double serviceExpense;
    double productExpense;

    public Visit() {
        super();
    }

    public Visit(String name, Date date) {
        this.customer = new Customer(name);
        this.date = date;
    }
    public Visit(Customer customer1, DateFormat dateFormat, int i, int j) {
        this.customer = customer1;
        this.date =  new Date();
        this.serviceExpense = i;
        this.productExpense = j;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getServiceExpense() {
        return serviceExpense;
    }

    public void setServiceExpense(double serviceExpense) {
        this.serviceExpense = serviceExpense;
    }

    public double getProductExpense() {
        return productExpense;
    }

    public void setProductExpense(double productExpense) {
        this.productExpense = productExpense;
    }

    // phương thức tính tổng chi phí dịch vụ
    public double getCost() {
        return this.serviceExpense + this.productExpense;
    }

    @Override
    public String toString() {
        return "Visit [customer=" + customer + ", date=" + date + ", productExpense=" + this.productExpense
                + ", serviceExpense=" + this.serviceExpense + ", getCost" + getCost() + "]";
    }

}
